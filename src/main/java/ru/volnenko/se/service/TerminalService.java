package ru.volnenko.se.service;

import ru.volnenko.se.api.service.ICommandProvider;
import ru.volnenko.se.api.service.ITerminalService;
import ru.volnenko.se.command.AbstractCommand;

import java.util.Collection;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Denis Volnenko
 */
@Service
public final class TerminalService implements ITerminalService {

    private final Scanner scanner = new Scanner(System.in);

    @Autowired
    private ICommandProvider commandProvider;

    public String nextLine() {
        return scanner.nextLine();
    }

    public Integer nextInteger() {
        final String value = nextLine();
        if (value == null || value.isEmpty()) return null;
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandProvider.getCommands();
    }
}
