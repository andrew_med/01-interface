package ru.volnenko.se.repository;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionType;

public abstract class AbstractRepository<T> {

    private final Class<T> entityClass;

    protected AbstractRepository(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
	
	protected abstract void merge(Collection<T> object);

	protected abstract Collection<T> values();

	@PostConstruct
	private void load() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			CollectionType type = mapper.getTypeFactory().constructCollectionType(List.class, entityClass);
			List<T> objects = mapper.readValue(new FileInputStream(getFileName()), type);
			merge(objects);
		} catch (IOException e) {
		    System.out.println(e.getMessage());
		}
	}
	
	@PreDestroy
	private void save() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			mapper.writeValue(new FileOutputStream(getFileName()), values());
		} catch (IOException e) {
		    System.out.println(e.getMessage());
		}
	}
	
	private String getFileName() {
		return entityClass.getSimpleName() + "s.json";
	}
}
