package ru.volnenko.se.api.service;

import ru.volnenko.se.command.AbstractCommand;

import java.util.Collection;

/**
 * @author Denis Volnenko
 */
public interface ICommandProvider {

    Collection<AbstractCommand> getCommands();
    
    void execute(String command) throws Exception;

}
