package ru.volnenko.se.api.service;

import ru.volnenko.se.command.AbstractCommand;

import java.util.Collection;

/**
 * @author Denis Volnenko
 */
public interface ITerminalService {

    String nextLine();

    Integer nextInteger();

    Collection<AbstractCommand> getCommands();

}
