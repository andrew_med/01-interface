package ru.volnenko.se.command;

import org.springframework.beans.factory.annotation.Autowired;

import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.api.service.ITerminalService;

/**
 * @author Denis Volnenko
 */
public abstract class AbstractCommand {

    @Autowired
    protected IProjectService projectService;

    @Autowired
    protected ITaskService taskService;

    @Autowired
    protected ITerminalService terminalService;

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;
}
